import React, { Component, useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image, TextInput } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


function newpassword({navigation}) {
    const [New,setNew]=useState()
    const [change,setchange]=useState()
    const onclick=()=>{
        navigation.navigate('ChangePassword',
        {New:New,
         change:change ,  
        }
        )


    }
    return (
        <View style={{ flex: 1, }}>
                <ImageBackground
                 resizeMode="stretch"
                 style={{ flex: 1, }}
                 source={require('../asstes/Images/bg_Two.png')}
            > 
                <TouchableOpacity onPress={() => navigation.goBack()}>
                     <Image
                         source={require('../asstes/Images/back_arrow.png')}
                        style={styles.image}
                     />
                </TouchableOpacity >
                <KeyboardAwareScrollView>
 
                 <View style={{ paddingHorizontal: 20 }}>
                     <Image style={styles.icon}
                         source={require('../asstes/Images/key_icon.png')}
                     />
                     <Text style={{ fontSize: 20, fontWeight: 'bold', alignSelf: 'center' }}>Please enter new password</Text>
                     <Text style={styles.old}>New Password</Text>
                     <TextInput style={styles.textinput}
                         placeholder="Enter New Password"
                         onChangeText={(text)=>{setNew(text)}}
                         value={New}

                     />
                     <Text style={styles.old}>Change Password</Text>
                    <TextInput style={styles.textinput}
                         placeholder="Enter Change Password"
                        onChangeText={(text)=>{setchange(text)}}
                         value={change}
                     />
                      <TouchableOpacity onPress={() => onclick()}
                         style={styles.opcity}
                    >
                         <Text style={styles.texts}>Change Password</Text>
                     </TouchableOpacity>

                 </View>
                 </KeyboardAwareScrollView>
            </ImageBackground>
         </View>
     );
} 
const styles = StyleSheet.create({
    image: {
        height: 30,
        width: 30,
        resizeMode: "center",
        paddingHorizontal: 40,
        marginVertical: 10
    },
    icon: {
        height: 150,
        width: 150,
        alignSelf: 'center',
        resizeMode: 'contain',
        marginVertical: 30
    },
    old: {
        marginVertical: 10,
        fontSize: 16,
        color: "grey"
    },
    textinput: {
        borderWidth: 1,
        backgroundColor: '#fff',
        borderRadius: 5,
        paddingHorizontal: 10
    },
    opcity: {
        backgroundColor: '#91bf3e',
        marginVertical: 50,
        borderRadius: 20,
        elevation: 2,
        marginTop: '50%'
    },
    texts: {
        padding: 10,
        textAlign: 'center',
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    },

})
export default newpassword;
