import React, { useState } from 'react';
import {
    View, Text, ImageBackground, Image, StyleSheet, TouchableOpacity,
    TextInput, ScrollView, FlatList, ActivityIndicator
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';

function AddNews({ navigation }) {
    const [selectCamer, setSelectCamer] = useState()
    const [title, setTitle] = useState()
    const [description, setDescription] = useState()
    const [tag, setTag] = useState();
    const [Animating, setAnimating] = useState(false)

    // const [userData, setUserData] = useState({
    //     image:"",
    //     title:"",
    //     tag:"",
    //     description:""
    // })


    const [dataList, setDataList] = useState([])

    const onclick = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: false,
        }).then(image => {
            setSelectCamer(image.path)
            console.log(image);
        });
    }

    const addTag = () => {
        let tempArray = [...dataList]
        tempArray.push(tag)
        console.log('-----res:' + JSON.stringify(tempArray))
        setDataList(tempArray)
        setTag("")
    }

    const tagRemove = (index) => {
        let array = [...dataList]
        if (index != -1) {
            array.splice(index, 1);
            setDataList(array)
        }
    }

    const saveData = () => {

        if (!selectCamer) {
            alert("Plz Select Image")
            return
        }
        if (!title) {
            alert("Plz Enter Title")
            return
        }
        if (dataList == "") {
            alert("Plz Select Tag")
            return
        }
        if (!description) {
            alert("Plz Enter description")
            return
        }
        setAnimating(true)
        setTimeout(() => {
            navigation.navigate("News",
                {
                    title: title,
                    selectCamer: selectCamer,
                    dataList: dataList,
                    description: description
                })
            setAnimating(false);
        }, 2000);

    }

    return (

        <View style={{ flex: 1 }}>
            <ImageBackground
                resizeMode="stretch"
                source={require('../asstes/Images/bg_Two.png')}
                style={{ flex: 1 }}
            >
                <ScrollView>
                    <View style={{ paddingHorizontal: 20 }}>
                        <View style={styles.image}>
                            <TouchableOpacity onPress={() => navigation.goBack()}
                            >
                                <Image
                                    source={require('../asstes/Images/back_arrow.png')}
                                    style={{ height: 30, width: 30, resizeMode: "center" }}
                                />
                            </TouchableOpacity>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Add News</Text>
                            <Text>
                            </Text>
                        </View>
                        <View style={{ height: 230, width: 320, backgroundColor: '#dfdfdf', justifyContent: 'center', alignItems: 'center', marginTop: 20, borderRadius: 10 }}>
                            <TouchableOpacity onPress={() => onclick()}
                            >
                                <Image source={selectCamer ? { uri: selectCamer } : require('../asstes/Images/camera-2.png')}
                                    style={{ height: 220, width: 600, resizeMode: "center" }}
                                />
                            </TouchableOpacity>

                        </View>
                        <ActivityIndicator animating={Animating}
                            size="small" color="red" />
                        <Text style={{ marginVertical: 10, marginTop: 30 }}>Title</Text>
                        <TextInput style={{ borderWidth: 1, borderRadius: 10, paddingHorizontal: 10 }}
                            placeholder="Enter Title "
                            keyboardType="default"
                            onChangeText={(text) => { setTitle({ text }) }}
                            value={title}
                        />
                        <Text style={{ marginVertical: 10, }}>Tags</Text>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center', justifyContent: "space-between" }}>
                            <TextInput style={{ flex: 1 }}
                                placeholder="Enter Tags"
                                keyboardType="default"
                                onChangeText={(text) => { setTag({ text }) }}
                                value={tag}
                            />
                            <TouchableOpacity onPress={() => addTag()}
                            >
                                <Image source={require('../asstes/Images/add-tag.png')}
                                    style={{ height: 20, width: 20, paddingHorizontal: 10 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={dataList}
                            numColumns={3}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => (
                                <View style={{ backgroundColor: '#91bf3e', width: 100, marginVertical: 5, borderRadius: 7, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                    <Text style={{ padding: 5, color: '#fff', }}>{item.text}</Text>
                                    <TouchableOpacity onPress={() => tagRemove(index)}>
                                        <Image
                                            source={require('../asstes/Images/close.png')}
                                            style={{ height: 15, width: 15, resizeMode: 'contain', paddingHorizontal: 13 }}
                                        />
                                    </TouchableOpacity>

                                </View>
                            )}
                        />
                        <Text style={{ marginTop: 40, }}>Description</Text>
                        <TextInput style={{ borderWidth: 1, borderRadius: 10, paddingHorizontal: 10, height: 150, textAlignVertical: "top" }}
                            placeholder="Enter Description"
                            keyboardType="default"
                            multiline={true}
                            onChangeText={(text) => { setDescription({ text }) }}
                            value={description}
                        />
                        <TouchableOpacity onPress={() => saveData()}
                            style={{ backgroundColor: '#91bf3e', marginVertical: 20, borderRadius: 25 }}>

                            <Text style={{ padding: 15, textAlign: 'center', color: '#fff' }}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    image: {
        flexDirection: 'row',
        marginTop: 40,
        justifyContent: 'space-between'
    },

})
export default AddNews;
