import React, { useState } from 'react';
import { View, Text, ImageBackground, StyleSheet, TextInput,  Image, TouchableOpacity, ScrollView, } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Tabs from '../navigastion/Tabs';
import {Picker} from '@react-native-picker/picker';



function Ownership({ navigation }) {
    const [selectImage, setSelectImage] = useState("");
    const button = () => {
        ImagePicker.openPicker({
            width: 100,
            height: 100,
            borderRadius: 100 / 2,
            cropping: true
        }).then(image => {
            setSelectImage(image.path)
            console.log(image);
        });

    }
    const [selectCamera, setSelectCamera] = useState('')
    const Camera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            setSelectCamera(image.path)
            console.log(image);
        });

    }
    const [selectedValue, setSelectedValue] = useState("java");

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                resizeMode='stretch'
                style={styles.bacg}
                source={require('../asstes/Images/bg.png')}
            >
                <View style={{ height: 100 }}>
                    <Tabs currentPosition={2} />

                </View>
                <ScrollView>
                    <View style={{ paddingHorizontal: 20,marginTop:20 }}>
                        <Text style={{ fontSize: 12, color: '#91bf3e' }}>step2 of 5</Text>
                        <Text style={{ marginVertical: 20, fontSize: 20, fontWeight: 'bold' }}>Farm Ownership</Text>
                        <Text style={{ marginVertical: 7 }}>Owner Nmae</Text>
                        <TextInput style={{ borderWidth: 1, borderRadius: 10, paddingHorizontal: 10 }}
                            placeholder="Enter Owner Nmae"
                            keyboardType="default"

                        />
                        <Text style={{ marginVertical: 7 }}>Gender</Text>
                        <View style={styles.flex}>
                            <TextInput style={{}}
                            // placeholder="setect Country"
                            />
                            <Picker selectedValue={selectedValue}
                                style={{ height: 50, width: 300 }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="setect Country" value="java" />
                                <Picker.Item label="Mala" value="js" />
                                <Picker.Item label="Mala" value="js" />

                            </Picker>

                        </View>
                        <Text style={{ marginVertical: 7 }}>Owner Nmae</Text>
                        <TextInput style={{ borderWidth: 1, borderRadius: 10, paddingHorizontal: 10 }}
                            placeholder="Enter Phone Nubmer"
                            keyboardType="number-pad"

                        />
                        <Text style={{ marginVertical: 7 }}>ID Copy</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <View>
                                <View style={styles.image}>
                                    <TouchableOpacity onPress={() => button()}
                                    >
                                        <Image source={selectImage ? { uri: selectImage } : require('../asstes/Images/plus.png')}
                                            style={{ height: 70, width: 70 }}
                                        />

                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.text}>Front</Text>
                            </View>
                            <View>
                                <View style={[styles.image, { marginLeft: 10 }]}>
                                    <TouchableOpacity onPress={() => Camera()}
                                    >
                                        <Image source={selectCamera ? { uri: selectCamera } : require('../asstes/Images/plus.png')}
                                            style={{ height: 70, width: 70 }}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.text}>Back</Text>
                            </View>

                        </View>
                        <Text style={{ marginVertical: 7 }}>Gender</Text>
                        <View style={styles.flex}>
                            <TextInput style={{}}
                            // placeholder="setect Country"
                            />
                            <Picker selectedValue={selectedValue}
                                style={{ height: 50, width: 300 }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="setect Country" value="java" />
                                <Picker.Item label="Mala" value="js" />
                                <Picker.Item label="Mala" value="js" />

                            </Picker>

                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 30 }}>
                            <TouchableOpacity onPress={() => navigation.goBack()}
                                style={[styles.opcity, { backgroundColor: 'black' }]}>
                                <Text style={styles.next}>Back</Text>

                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate("Tayp")}
                                style={styles.opcity}>
                                <Text style={styles.next}>Next</Text>

                            </TouchableOpacity>

                        </View>

                    </View>
                </ScrollView>
            </ImageBackground>

        </View>
    );
}
const styles = StyleSheet.create({
    bacg: {
        flex: 1,
        marginVertical: 10
    },
    flex: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: 'grey'
    },
    image: {
        borderWidth: 1,
        height: 100,
        width: 90,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        textAlign: 'center',
        fontSize: 16,
        color: 'grey'
    },
    opcity: {
        backgroundColor: '#91bf3e',
        width: '35%',
        borderRadius: 25,
        elevation: 3,
        marginVertical: 10,
    },
    next: {
        padding: 15,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center',
        fontWeight: 'bold'
    },
})
export default Ownership;
