import React, { useState } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Image, StyleSheet, TextInput, ActivityIndicator } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

function ChangePassword({ navigation, route }) {
  const [Animating, setAnimating] = useState(false)
  const [secureEntry, setSecureEntry] = useState(true)
  const [secureEntrys, setSecureEntrys] = useState(true)
  const [toggel, setToggel] = useState()
  const [toggels, setToggels] = useState()
  const Password = () => {
    setToggel(!toggel)
    setSecureEntry(secureEntry => (!secureEntry))
  }
  const Passwords = () => {
    setToggels(!toggels)
    setSecureEntrys(secureEntrys => (!secureEntrys))
  }
  const onclick = () => {
    setAnimating(true)
    setTimeout(() => {
      navigation.navigate("newpassword")
      setAnimating(false);
    }, 1000);
  }
  return (
    <View style={{ flex: 1, }}>
      <ImageBackground
        resizeMode="stretch"
        style={{ flex: 1, }}
        source={require('../asstes/Images/bg.png')}
      >
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image
            source={require('../asstes/Images/back_arrow.png')}
            style={styles.image}
          />
        </TouchableOpacity >
        <KeyboardAwareScrollView>
          <View style={{ paddingHorizontal: 20 }}>
            <ActivityIndicator animating={Animating}
              size="small" color="#0000ff" />
            <Text style={styles.text}>Change Password</Text>
            <Text style={styles.old}> Old Password  </Text>
            <TextInput style={styles.textinput}
              placeholder="Enter Old Password"
            />
            <Text style={styles.old}> New Password </Text>
            <View style={[styles.textinput, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
              <TextInput style={{ flex: 1, }}
                placeholder=
                {route.params ? route.params.New : "New Password"}
                secureTextEntry={secureEntry}

              />
              <TouchableOpacity onPress={() => Password()}>
                <Image style={{ height: 20, width: 28 }}
                  source={toggel == false ? require('../asstes/Images/eye-off.png')
                    : require('../asstes/Images/eye.png')}
                />
              </TouchableOpacity>

            </View>

            <Text style={styles.old}>Change Password</Text>
            <View style={[styles.textinput, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}>
              <TextInput style={{ flex: 1, }}
                placeholder=
                {route.params ? route.params.change : 'Change Password'}
                secureTextEntry={secureEntrys}
              />
              <TouchableOpacity onPress={() => Passwords()}>
                <Image style={{ height: 20, width: 28 }}
                  source={toggels == false ? require('../asstes/Images/eye-off.png')
                    : require('../asstes/Images/eye.png')}
                />
              </TouchableOpacity>

            </View>
            <TouchableOpacity onPress={() => onclick()}
              style={styles.opcity}
            >
              <Text style={styles.texts}>Change Password</Text>
            </TouchableOpacity>

          </View>
        </KeyboardAwareScrollView>
      </ImageBackground>

    </View>
  );
}
const styles = StyleSheet.create({
  image: {
    height: 30,
    width: 30,
    resizeMode: "center",
    paddingHorizontal: 40,
    marginVertical: 10
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 10
  },
  old: {
    marginVertical: 10,
    fontSize: 16,
    color: "grey"
  },
  textinput: {
    borderWidth: 1,
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingHorizontal: 10
  },
  opcity: {
    backgroundColor: '#91bf3e',
    marginVertical: 50,
    borderRadius: 20,
    elevation: 2,
    marginTop: '50%'
  },
  texts: {
    padding: 10,
    textAlign: 'center',
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold'
  },


})
export default ChangePassword;
