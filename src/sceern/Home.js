import React, { useState } from 'react';
import {
    View, Text, ImageBackground, StyleSheet, Image,
    TouchableOpacity, ScrollView, FlatList, Modal
} from 'react-native';
// import Header from '../constants/Header';
import OptionsMenu from "react-native-option-menu";
import { screenWidth } from '../constants/Sizes.constant';
import Header from '../Widget/Header';


function Home({ navigation }) {
    const [modalVisible, setModalVisible] = useState(false);
    const [data,setData] = useState  ([
        { id: '1', image: require('../asstes/Images/farm.png'), text: 'Farm Registration', routes: 'Registra' },
        { id: '2', image: require('../asstes/Images/farmer.png'), text: 'Farmer Registration', routes: "fars" },
        { id: '3', image: require('../asstes/Images/notification.png'), text: 'Notification' },
        { id: '4', image: require('../asstes/Images/report.png'), text: 'Report' },
        { id: '5', image: require('../asstes/Images/purchase-icon.png'), text: 'Purchase' },
        { id: '6', image: require('../asstes/Images/news.png'), text: 'News', routes: 'News' },
    ])
    const onClick = (routes) => {
        if (routes === 'Registra') {
            navigation.navigate("Registra")
        }
        if (routes === "fars") {
            navigation.navigate("FarmerRegistration")
        }
        if (routes === "News") {
            navigation.navigate("News")
        }
    }
    const MoreIcon = require("../asstes/Images/Home.png");
    const editNewgroup = () => {
        navigation.navigate("EditProfile")
    }
    const Newbroadcast = () => {
    }
    const LinkadDevices = () => {
    }
    const Starredmessages = () => {
    }
    const Payments = () => {
    }
    const Settings = () => {
        navigation.navigate("Profil")
    }
    // const onClick = () => {
    //     setShowSearchBar(!showSearchBar)
    // }

    return (
        <>
            <Header headerCenter={true} leftButttonType='noIcon' title='Home'  />
            <View style={{ flex: 1 }}>
                <ImageBackground
                    resizeMode="stretch"
                    source={require('../asstes/Images/bg_Two.png')}
                    style={{ flex: 1 }}
                >
                    <View style={{ paddingHorizontal: 20 }}>
                        <View style={styles.home}>
                            <OptionsMenu
                                button={MoreIcon}
                                buttonStyle={{ width: 30, height: 20, margin: 7.5, resizeMode: "contain" }}
                                destructiveIndex={1}
                                options={
                                    [
                                        "EditProfile",
                                        "New broadcast",
                                        "Linkad Devices",
                                        "Starred messages ",
                                        "Payments",
                                        "Settings",
                                        "",
                                    ]
                                }
                                actions={[editNewgroup, Newbroadcast, LinkadDevices, Starredmessages, Payments, Settings]}
                            />
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Home</Text>
                            <TouchableOpacity onPress={() => navigation.navigate('Account')}
                            >
                                <Image
                                    source={require('../asstes/Images/profile_user.png')}
                                    style={{ height: 30, width: 30 }}
                                />

                            </TouchableOpacity>
                        </View>
                            <View style={styles.home}>
                                <Text style={{ fontSize: 20, fontWeight: 'bold', }}>Latest News</Text>
                                <TouchableOpacity onPress={() => setModalVisible(true)}
                                    style={styles.opcity}>
                                    <Text style={{ color: '#91bf3e', padding: 8, }}>View All</Text>
                                </TouchableOpacity>
                            </View>

                            <ScrollView horizontal={true}> 
                                <ImageBackground
                                    source={require('../asstes/Images/images.jpeg')}
                                    style={styles.image} >
                                    <Text style={styles.text}>Lorem Ipsum is simply dummy text of the printing</Text>
                                    <TouchableOpacity style={styles.life}>
                                        <Text style={{ color: '#fff', padding: 7, alignItems: 'center' }}>Farmers Life</Text>
                                    </TouchableOpacity>
                                </ImageBackground>
                                <ImageBackground
                                    source={require('../asstes/Images/images.jpeg')}
                                    style={styles.image} >
                                    <Text style={styles.text}>Lorem Ipsum is simply dummy text of the printing</Text>
                                </ImageBackground>
                                <ImageBackground
                                    source={require('../asstes/Images/images.jpeg')}
                                    style={styles.image} >
                                    <Text style={styles.text}>Lorem Ipsum is simply dummy text of the printing</Text>
                                </ImageBackground>
                                </ScrollView>
                            <View style={[{ marginTop: 30, marginBottom:20}]}>
                                <FlatList
                                    data={data}
                                    numColumns={2}
                                    keyExtractor={item => item.id}
                                    renderItem={({ item, index }) => (
                                        <TouchableOpacity onPress={() => { onClick(item.routes) }}
                                            style={styles.Lastitme}>
                                            <Image
                                                source={item.image}
                                                style={{ height: 50, width: 50, resizeMode: 'contain' }}
                                            />
                                            <Text style={styles.textitem}>{item.text}</Text>
                                        </TouchableOpacity>
                                    )}
                                />
                            </View>
                    </View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            Alert.alert("Modal has been closed.");
                            setModalVisible(!modalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={{
                                backgroundColor: "#fff", width: 230, padding: 15,
                                borderRadius: 5, elevation: 5
                            }}>
                                <View style={styles.modalView}>
                                    <Text />
                                    <Text style={styles.modalText}>Purchase</Text>
                                    <TouchableOpacity
                                        style={[styles.button, styles.buttonClose]}
                                        onPress={() => setModalVisible(!modalVisible)}
                                    >
                                        <Image source={require('../asstes/Images/close-circle-outline.png')}
                                            style={{ height: 20, width: 20 }}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 30, width: 200 }}>
                                    <TouchableOpacity onPress={() => navigation.navigate('MilkCollection')}
                                        style={styles.milk}>
                                        <Image source={require('../asstes/Images/milk.png')}
                                            style={styles.modalimage}
                                        />
                                        <Text style={{ textAlign: 'center', fontSize: 10 }}>Milk Collection</Text>
                                    </TouchableOpacity >
                                    <View style={styles.milk}>
                                        <TouchableOpacity onPress={() => navigation.navigate("Device")}
                                        >
                                            <Image source={require('../asstes/Images/meat-sourcing.png')}
                                                style={styles.modalimage}
                                            />
                                        </TouchableOpacity>
                                        <Text style={{ textAlign: 'center', fontSize: 10 }}>Meat Sourcing</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </ImageBackground>
            </View>
        </>
    );
}
const styles = StyleSheet.create({
    home: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: 40, marginTop: 20
    },
    opcity: {
        borderWidth: 1,
        borderColor: '#91bf3e',
        borderRadius: 5
    },
    image: {
        height: 200,
        width: 320,
        resizeMode: 'cover',
        paddingHorizontal: 2,
        borderRadius: 10
    },
    text: {
        color: '#fff',
        fontSize: 18,
        padding: 20,
        fontWeight: 'bold',
        marginTop: 50
    },
    life: {
        backgroundColor: '#91bf3e',
        width: 120,
        paddingHorizontal: 10,
        height: 40,
        marginLeft: 15,
        borderRadius: 10
    },
    Lastitme: {
        width: '50%',
        alignItems: 'center',
        height: 100,
        elevation: .7,
        borderColor: 'black',
        justifyContent: 'center'
    },
    textitem: {
        fontSize: 12,
        marginVertical: 5,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",

    },
    modalView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    milk: {
        alignItems: 'center',
        elevation: 2, backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
    },
    modalimage: {
        height: 60,
        width: 60,
        resizeMode: 'contain'
    },
})
export default Home;
