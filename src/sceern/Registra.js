import React, { useState } from 'react';
import { View, Text, ImageBackground, StyleSheet, Image, TextInput, ScrollView, TouchableOpacity, Button } from 'react-native';
import Tabs from '../navigastion/Tabs';
import {Picker} from '@react-native-picker/picker';


function Registra({navigation,}) {
    const [selectedValue, setSelectedValue] = useState("java");
    const [toggol,setToggol]=useState(false) 
   const onclick=(toggol)=>{
    navigation.navigate("Ownership")
    setToggol(!toggol)
    
   }
    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                resizeMode='stretch'
                source={require('../asstes/Images/bg_Two.png')}
                style={styles.bacgi}
            >
                <View style={{height:100}}>
                <Tabs   currentPosition={1}/>

                </View>
                <ScrollView>
                    <View style={{ paddingHorizontal: 20, marginTop:20}}>
                        <Text style={{ fontSize: 12, color: '#91bf3e' }}>step1 of 5</Text>
                        <Text style={{ marginVertical: 20, fontSize: 20, fontWeight: 'bold' }}>Farm Registrtion</Text>
                        <Text style={{ marginVertical: 7 }}>Country </Text>
                        <View style={styles.flex}>
                            <TextInput style={{}}
                            // placeholder="setect Country"
                            />
                            <Picker selectedValue={selectedValue}
                                style={{ height: 50, width: 300 }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="setect Country" value="java" />
                                <Picker.Item label="Mala" value="js" />
                            </Picker>
                        </View>
                        <Text style={{ marginVertical: 7 }}>Country</Text>
                        <View style={styles.flex}>
                            <TextInput style={{}}
                            // placeholder="setect Country"
                            />
                            <Picker selectedValue={selectedValue}
                                style={{ height: 50, width: 300 }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="setect Country" value="java" />
                                <Picker.Item label="hfbsh" value="js" />
                            </Picker>
                        </View>
                        <Text style={{ marginVertical: 7 }}>Sub-Country</Text>
                        <View style={styles.flex}>
                            <TextInput style={{}}
                            // placeholder="setect Country"
                            />
                            <Picker selectedValue={selectedValue}
                                style={{ height: 50, width: 300 }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="setect Country" value="java" />
                                <Picker.Item label="Sub-Country" value="js" />
                            </Picker>
                        </View>
                        <Text style={{ marginVertical: 7 }}>Ward</Text>
                        <View style={styles.flex}>
                            <TextInput style={{}}
                            // placeholder="setect Country"
                            />
                            <Picker selectedValue={selectedValue}
                                style={{ height: 50, width: 300 }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="setect Country" value="java" />
                                <Picker.Item label="Ward" value="js" />
                            </Picker>
                        </View>
                        <Text style={{ marginVertical: 7 }}>Home area</Text>
                        <TextInput style={styles.textinput}
                        />
                        <Text style={{ marginVertical: 7 }}>Route</Text>
                        <View style={styles.flex}>
                            <TextInput style={{}}
                            // placeholder="setect Country"
                            />
                            <Picker selectedValue={selectedValue}
                                style={{ height: 50, width: 300 }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="setect Country" value="java" />
                                <Picker.Item label="Route" value="js" />
                            </Picker>
                        </View>
                        <TouchableOpacity onPress={() =>onclick(toggol)}
                            style={styles.opcity}>
                            <Text style={styles.next}>Next</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    bacgi: {
        flex: 1,
    },
    flex: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: 'grey'
    },
    opcity: {
        backgroundColor: '#91bf3e',
        width: '30%',
        marginVertical: 40,
        borderRadius: 20,
        alignSelf: 'flex-end',
        elevation: 3
    },
    next: {
        padding: 10,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    textinput: {
        borderWidth: 1,
        height: 100,
        borderRadius: 10,
        borderColor: 'grey'
    },
})
export default Registra;
