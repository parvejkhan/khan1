import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Item } from 'react-native-paper/lib/typescript/components/List/List';



function SalMonella(props) {
    const { products, backScreen } = props.route.params
    const [data, setData] = useState('')
    const [animating, setAnimating] = useState(false)

    useEffect(() => {
        setAnimating(true)
        const getAccountLists = async () => {
            let dataS = {
                data: products
            }
            let item = await AsyncStorage.getItem('token');
            if (result.status) {
                setAnimating(false)
                setData(result.products[0])
            }
            getUserData(item)
        }
        getAccountLists()

    }, [])


    const [date, setDate] = useState("");
    const [count, setCount] = useState(0);

    const [toggel, setToggel] = useState(false)
    const onclick = () => {
        setToggel(!toggel)
    }




    return (
        <View style={{ flex: 1, }}>
            <ImageBackground
                resizeMode="stretch"
                source={require('../asstes/Images/bg_Two.png')}
                style={{ flex: 1, }}
            >
                <View style={{ paddingHorizontal: 10 }}>
                    <View style={{ flexDirection: 'row', marginVertical: 20 }}>
                        <TouchableOpacity onPress={() => props.navigation.goBack()}>
                            <Image style={{ height: 30, width: 30, resizeMode: 'contain' }}
                                source={require('../asstes/Images/back_arrow.png')}
                            />
                        </TouchableOpacity>
                        <View style={{ marginLeft: 30 }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Sal Monella</Text>
                            <Text style={{ fontSize: 12 }}>2 Junction Mall, Dagoretti</Text>

                        </View>

                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <DatePicker
                            style={styles.datePickerStyle}
                            date={date} // Initial date from state
                            // mode="date" // The enum of date, datetime and time
                            placeholder="select date"
                            format="DD-MM-YYYY"
                            minDate="01-01-2000"
                            maxDate="01-08-2021"
                            // confirmBtnText="Confirm"
                            // cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    // display: 'none',
                                    position: 'absolute',
                                    left: 0,

                                },
                                dateInput: {
                                    marginRight: 0,
                                    width: 200
                                },
                            }}
                            onDateChange={(date) => {
                                setDate(date);
                            }}
                        />

                    </View>
                    <View style={{ backgroundColor: '#e4f5ea', alignItems: 'center', flexDirection: 'row', width: 330, padding: 10, marginVertical: 20, }}>
                        <View style={{ flexDirection: 'row', marginLeft: 50 }}>
                            <TouchableOpacity onPress={() => onclick()}
                            >
                                <Image style={{ height: 20, width: 20, }}
                                    source={toggel == false ? require('../asstes/Images/chcak.png') :
                                        require('../asstes/Images/checked.png')}
                                />
                            </TouchableOpacity>
                            <Text style={{ marginLeft: 10 }}>Mornimgef</Text>

                        </View>
                        <View style={{ flexDirection: 'row', marginLeft: 20 }}>
                            <TouchableOpacity onPress={() => onclick()}
                            >
                                <Image style={{ height: 20, width: 20, }}
                                    source={toggel == true ? require('../asstes/Images/chcak.png') :
                                        require('../asstes/Images/checked.png')}
                                />
                            </TouchableOpacity>
                            <Text style={{ marginLeft: 10 }}>Evening</Text>

                        </View>



                    </View>
                    <ScrollView>
                    <View>
                        <Image style={[styles.image, { resizeMode: 'contain' }]}
                            source={{ uri: products.thumb }}
                        />
                        <Text style={{ fontSize: 20, }}>{products.name}</Text>
                        <Text style={{ fontSize: 16, }}>  {products. price} </Text>
                        <Text style={{ fontSize:14, }}>{products.description}</Text>
                    </View>
                  <Text>{2*count}</Text>
                    </ScrollView>

<TouchableOpacity  onPress={() => {setCount(count + 1)}}>
    <Text style={{marginLeft:10,marginTop:20}}> count {count}</Text>
</TouchableOpacity>



                </View>

            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    datePickerStyle: {
        width: 180,

    },
    image: {
        height: 200,
        width: 300,
        marginTop: 10,
    },
})
export default SalMonella;
