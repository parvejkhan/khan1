import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, StyleSheet, Image, TouchableOpacity, ActivityIndicator, TextInput, Linking } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';
import axios from 'axios'
import { ApiUrl } from './Config/urls';
function Login(props) {
    // const { backScreen } = props.route.params
    const [secure, setSecure] = useState(true)
    const [toggel, setToggel] = useState(false)
    const [errortext, setErrortext] = useState('');
    const button = () => {
        setToggel(!toggel)
        setSecure(secure => (!secure))
    }
    const [toggles, setToggels] = useState(false)
    const chack = () => {
        setToggels(!toggles)
    }
    const [validatedEmail, setValidatedEmail] = useState()
    const [password, setPassword] = useState()
    const [Animating, setAnimating] = useState(false)
    let result = {
        email: validatedEmail,
        Password: password
    }
    useEffect(() => {
        onclick()
    }, [2000])
    const productList = () => {
        axios.get('https://bitglobal.bngvogue.com/opencart/index.php?route=api/product').then((resp => {
            var resp = resp
            alert(productList)
            console.log('resp', resp)
        }))
        props.navigation.navigate('Home')
    }
    // console.log('productList', productList)

    const onclick = async () =>  {
        // let error = false, errorMsg = ''
        // setErrortext('');

        // if (!email) {
        //     // setAnimating(true)
        //     alert('Please enter your Email Id')
        //     // return
        // }
        // else if (!password) {
        //     alert('Please enter your password')
        //     return
        // }
        // else if (!toggles) {
        //     alert('Please chack verfi')
        //     return
        // }
        // let formData = new FormData()
        // formData.append('email', email)
        // formData.append('password', password)

        // console.log('formData', formData)
        // axios.post(ApiUrl.baseUrl+ApiUrl.login, formData).then((resp => {
        //     console.log('resp', resp.data)
        //     var result = resp.data
        //     Toast.show({ type: 'success', text1: result.status })
        //     alert(result.status)
        //     console.log('result', result.status)
        //     if (result.status) {
        //         props.navigation.navigate('Home')
        //     }

        // }))
        setErrortext('');
        if (!validatedEmail) {
            Toast.show({ type: 'error', text1: 'Please Enter E-mail Address' })
            return;
        }
        if (!password) {
            Toast.show({ type: 'error', text1: 'Please Enter Password' })
            return;
        }
        let formData = new FormData()
        formData.append('email', validatedEmail)
        formData.append('password', password)

        console.log('formData', formData)
        await axios.post(ApiUrl.baseUrl+ApiUrl.login, formData).then((resp => {
            console.log('resp', resp.data)
            var result = resp.data
            Toast.show({ type: 'success', text1: result.status })
            alert(result.status)
            console.log('result', result.status)
            if (result.status == true) {
                props.navigation.navigate('Home')
            }
            props.navigation.navigate('Home')

        }))
    }
    const PhoneCall = phone => {
        console.log('callNumber ----> ', 7425052022);
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${'7425052022'}`;
        }
        else {
            phoneNumber = `tel:${'7425052022'}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    Alert.alert('Phone number is not available');
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
    }
    return (
        <View style={{ flex: 1 }}>
            {/* <View style={{ margin: 30 }}>
                <TextInput
                    label='Email address'
                    mode='outlined'
                    theme={{ colors: { primary: "red", underlineColor: "transparent", } }}
                    underlineColor={null}
                    placeholder=' email-address '
                    keyboardType='email-address'
                    returnKeyType={"next"}
                    underlineColorAndroid='transparent'
                    autoCapitalize='none'
                    value={text}
                    onChangeText={text => setText(text)}
                />
            </View> */}
            <ImageBackground
                source={require('../asstes/Images/new_bg.png')}
                style={styles.bacgi} >
                <KeyboardAwareScrollView>
                    <View style={styles.manview}>
                        <Image
                            source={require('../asstes/Images/logo-white.png')}
                            style={styles.image}
                        />
                    </View>
                    <View style={styles.sinde}>
                        <ActivityIndicator animating={Animating}
                            size="small" color="red" />
                        <Text style={styles.login}>Login</Text>
                        <View style={{ paddingHorizontal: 20 }}>
                            <View style={styles.row}>
                                <Image source={require('../asstes/Images/email.png')}
                                    style={styles.email}
                                />
                                <TextInput style={{ flex: 1 }}
                                    placeholder="Email Adderss"
                                    keyboardType="email-address"
                                    returnKeyType="next"
                                    onChangeText={(text) => { setValidatedEmail({ text }) }}
                                    value={validatedEmail}
                                />
                            </View>
                            <View style={[styles.row, { justifyContent: 'space-between' }]}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={require('../asstes/Images/open-lock.png')}
                                        style={styles.email}
                                    />
                                    <TextInput style={{ flex: 1 }}
                                        placeholder="Password"
                                        keyboardType="default"
                                        returnKeyType="next"
                                        secureTextEntry={secure}
                                        onChangeText={(text) => { setPassword({ text }) }}
                                        value={password}
                                    />
                                    <TouchableOpacity onPress={() => button()}
                                    >
                                        <Image style={{ height: 20, width: 28 }}
                                            source={toggel == false ? require('../asstes/Images/eye-off.png')
                                                : require('../asstes/Images/eye.png')}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <TouchableOpacity onPress={() => PhoneCall()}
                                style={{ height: 40, justifyContent: 'center' }}>
                                <Text style={{ color: 'black' }}>Call</Text>
                            </TouchableOpacity>
                            <View style={styles.chack}>
                                <TouchableOpacity onPress={() => chack()}>
                                    <Image source={toggles == false ? require('../asstes/Images/chcak.png')
                                        : require('../asstes/Images/checked.png')}
                                        style={{ height: 20, width: 20 }}
                                    />
                                </TouchableOpacity>
                                <Text style={{ marginLeft: 7 }}>Remember Me</Text>
                                <Text style={styles.forogot}>Forogot Password</Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => onclick()}
                                //  onPress={() => props.navigation.navigate("Home")}

                                style={styles.opcity}
                            >
                                <Text style={styles.text}>SIGN IN</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => props.navigation.navigate('RegisterSIGN')}
                                style={styles.opcity} >
                                <Text style={styles.text}>SIGN UP</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    bacgi: {
        flex: 1
    },
    image: {
        height: 150,
        width: 150
    },
    manview: {
        height: 250,
        alignItems: 'center',
        justifyContent: 'center'
    },
    sinde: {
        backgroundColor: '#fff',
        width: '80%',
        alignSelf: 'center',
        elevation: 4,
        borderRadius: 10,
        // height: 350
    },
    login: {
        textAlign: 'center',
        fontSize: 20,
        marginVertical: 10,
        fontWeight: 'bold'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: .6,
        width: '90%',
        marginVertical: 1
    },
    email: {
        height: 25,
        width: 25,
        resizeMode: 'contain'
    },
    chack: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 25
    },
    forogot: {
        marginLeft: 15,
        fontSize: 12,
        color: '#91bf3e'
    },
    opcity: {
        backgroundColor: '#91bf3e',
        marginVertical: 20,
        borderRadius: 20,
        elevation: 4
    },
    text: {
        padding: 10,
        textAlign: 'center',
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    },
})
export default Login;

// import React, { useRef, useState, useEffect } from 'react';
// import axios from 'axios';
// import { View, Text, StyleSheet, TouchableOpacity, Image, Linking, TextInput } from 'react-native';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
// import Toast from 'react-native-toast-message';
// import { ApiUrl } from './Config/urls';
// import { ColorsConstant } from '../constants/Colors.constant';
// import { screenWidth } from '../constants/Sizes.constant';
// import { StyleConstants } from '../constants/Style.constant';
// // import Header from '../../../Widget/Header'
// function LoginScreen(props) {
//     useEffect(() => {
//         login()
//     }, [2000])

//     const [disableInput, setDisableInput] = useState(false)
//     const [password, setPassword] = useState('')
//     const [validatedEmail, setValidatedEmail] = useState(false)
//     const [secure, setSecure] = useState(true)
//     const [toggle, setToggle] = useState()
//     const [errortext, setErrortext] = useState('');

//     const securityCheck = () => {
//         setToggle(!toggle)
//         setSecure(!secure)
//     }
//     const inputE2 = useRef(null);

//     const productList = () => {
//         axios.get('https://bitglobal.bngvogue.com/opencart/index.php?route=api/product').then((resp => {
//             var resp = resp
//             console.log('resp', resp)
//         }))
//         props.navigation.navigate('HomeScreen')
//     }
//     // console.log('productList', productList)

//     // const login = async () => {
//     //     setErrortext('');            const login  = async ()=>{}
//     //     if (!validatedEmail) {
//     //         alert('Please Enter Valid E-mail Address');
//     //         // Toast.show ({type:'error',text1:'Enter Valid E-mail Address'})
//     //         return;
//     //     }
//     //     if (!password) {
//     //         alert('Please Enter Valid Password');
//     //         return;
//     //     }
//     //     await axios.post('https://bitglobal.bngvogue.com/opencart/index.php?route=api/login',loginUser).then((resp => {
//     //         console.log('resp',resp.data)
//     //         var result = resp.data
//     //     }))
//     //     let loginUser = {
//     //         email: validatedEmail,
//     //         password: password,
//     //     }
//     //     console.log('loginUser', loginUser)
//     // }
//     const login = async () => {
//         setErrortext('');
//         if (!validatedEmail) {
//             Toast.show({ type: 'error', text1: 'Please Enter E-mail Address' })
//             return;
//         }
//         if (!password) {
//             Toast.show({ type: 'error', text1: 'Please Enter Password' })
//             return;
//         }
//         let formData = new FormData()
//         formData.append('email', validatedEmail)
//         formData.append('password', password)

//         console.log('formData', formData)
//         await axios.post(ApiUrl.baseUrl+ApiUrl.login, formData).then((resp => {
//             console.log('resp', resp.data)
//             var result = resp.data
//             Toast.show({ type: 'success', text1: result.status })
//             alert(result.status)
//             console.log('result', result.status)
//             if (result.status == true) {
//                 props.navigation.navigate('HomeScreen')
//             }

//         }))

//     }

//     return (
//         <>
//             {/* <Header leftButtonType='back' title='Login'/> */}
//             <View style={ls.mainView}>
//                 <KeyboardAwareScrollView>
//                     {/* <Image source={require('../../assets/Image/Exe-logo.png')} style={{height:100,width:100,alignSelf:'center'}} /> */}
//                     <View style={ls.manview}>

//                         <View style={ls.textinputView}>
//                             <TextInput
//                                 style={[ls.Textinput, { flex: 1, width: 200, }]}
//                                 placeholder="Enter your email address"
//                                 underlineColorAndroid="gray"
//                                 color={ColorsConstant.White}
//                                 keyboardType='email-address'
//                                 onChangeText={(e) => setValidatedEmail(e)}
//                                 editable={!disableInput}
//                                 returnKeyType="next"
//                                 onSubmitEditing={() => inputE2.current.focus()}
//                                 placeholderTextColor={ColorsConstant.White}
//                             />
//                         </View>

//                         <View style={[ls.Textinput, { flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 7, }]}>
//                             <TextInput style={{ flex: 1 }}
//                                 placeholder="Password"
//                                 color={ColorsConstant.White}
//                                 underlineColorAndroid="gray"
//                                 keyboardType='default'
//                                 secureTextEntry={secure}
//                                 maxLength={15}
//                                 onChangeText={(e) => setPassword(e)}
//                                 returnKeyType="next"
//                                 ref={inputE2}
//                                 placeholderTextColor={ColorsConstant.White}
//                             />
//                             <TouchableOpacity style={ls.Toucheye} onPress={() => securityCheck()}>
//                                 <Image source={toggle ? require('../asstes/Images/bg_Two.png') : require('../asstes/Images/bg_Two.png')}
//                                     style={ls.eyePic} />
//                             </TouchableOpacity>
//                         </View>
//                         <TouchableOpacity
//                             onPress={() => login()}
//                             style={ls.btnTheme}  >
//                             <Text style={s.textsigup}>Login</Text>
//                         </TouchableOpacity>

//                         <TouchableOpacity onPress={() => props.navigation.navigate('ForgotPassword')} style={ls.touchForgot}>
//                             <Text style={ls.textForgot}>Forgot Password ?</Text>
//                         </TouchableOpacity>

//                         <TouchableOpacity
//                             // onPress={()=>props.navigation.navigate('Theme')}
//                             onPress={() => props.navigation.navigate('RegisterScreen')}
//                             style={ls.touchForgot}>
//                             <Text style={ls.textForgot}>Not Registered ? Register Now !</Text>
//                         </TouchableOpacity>
//                         <TouchableOpacity onPress={() => props.navigation.navigate('EditProfile')} style={ls.touchForgot}>
//                             <Text style={ls.textForgot}>Edit Profile</Text>
//                         </TouchableOpacity>
//                         <View style={ls.roundView}>
//                             <Text style={{ textAlign: 'center' }}></Text>
//                         </View>
//                     </View>
//                     <View style={{justifyContent:'center',alignItems:'center'}}>
//                         <TouchableOpacity onPress={()=>props.navigation.navigate('TestingLogin')}
//                             style={ls.btnTheme}  >
//                             <Text style={s.textsigup}>Login</Text>
//                         </TouchableOpacity>
//                     </View>
//                 </KeyboardAwareScrollView>
//             </View>
//         </>
//     );
// }
// const s = StyleConstants, c = ColorsConstant, ls = StyleSheet.create({
//     manview: {
//         marginTop: 100,
//         backgroundColor: ColorsConstant.theme,
//         // height: screenHeight - 500,
//         height: 330,
//         width: screenWidth - 40,
//         alignContent: 'center',
//         alignSelf: 'center',
//         paddingHorizontal: 20,
//         justifyContent: 'center'
//     },
//     mainView: {
//         flex: 1,
//         backgroundColor: ColorsConstant.Black,
//     },
//     btnTheme: {
//         backgroundColor: ColorsConstant.btn,
//         padding: 16,
//         borderRadius: 10,
//         marginVertical: 20,
//         width: screenWidth - 80
//     },
//     textinputView: {
//         flexDirection: 'row',
//         position: 'relative',
//         alignItems: 'center'
//     },
//     roundView: {
//         backgroundColor: ColorsConstant.theme,
//         height: 90,
//         width: 100,
//         borderRadius: 50,
//         alignSelf: 'center',
//         position: 'absolute',
//         top: -55,
//     },
//     Toucheye: {
//         width: 35,
//         height: 50,
//         alignItems: 'center',
//         justifyContent: 'center'
//     },
//     eyePic: {
//         height: 25,
//         width: 25,
//         tintColor: ColorsConstant.AshGray,
//         marginRight: 50
//     },
//     touchForgot: {
//         flexDirection: 'row',
//         alignItems: 'center',
//         justifyContent: 'center'
//     },
//     textForgot: {
//         color: ColorsConstant.White,
//         fontSize: 12,
//         fontWeight: 'bold',
//         marginLeft: 10,
//         marginRight: 10
//     },
//     Textinput: {
//         paddingHorizontal: 10,
//         marginVertical: 15,
//         width: screenWidth - 40,
//     },
// })
// export default LoginScreen;


