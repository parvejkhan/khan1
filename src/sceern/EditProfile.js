import React, { useState } from 'react';
import { View, Text, ImageBackground, StyleSheet, TouchableOpacity, Image, TextInput, FlatList } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-crop-picker';



function EditProfile({ navigation }) {
    const [Surname, setSurname] = useState()
    const [Name, setName] = useState()
    const [Country, setCountry] = useState()
    const [selectImage, setSelectImage] = useState('')
    const[email,setEmail]=useState()
    const camer = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: false,
        }).then(image => {
            setSelectImage(image.path)
            console.log(image);
        });

    }


    const onclick = () => {

        if (!Surname) {
            alert('plz Enter Surname')
            return
        }
        if (!Name) {
            alert('plz Enter Nmae')
            return
        }

        if (!Country) {
            alert('plz Enter Nmae')
            return
        }
        navigation.navigate('Account', {
            Surname: Surname,
            Email: email,
            selectImage:selectImage,

        
        })

    }

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                resizeMode='stretch'
                style={{ flex: 1 }}
                source={require('../asstes/Images/bg.png')}
            >
                <KeyboardAwareScrollView>

                    <View style={{ paddingHorizontal: 20 }}>
                        <View style={styles.image}>
                            <TouchableOpacity onPress={() => navigation.goBack()}
                            >
                                <Image
                                    source={require('../asstes/Images/back_arrow.png')}
                                    style={{ height: 30, width: 30, resizeMode: "center" }}
                                />
                            </TouchableOpacity >
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Edit Prifile</Text>
                            <Text></Text>
                        </View>
                        <View style={{ alignSelf: 'center' }}>
                            <Image style={styles.manimage}
                                source={selectImage ? { uri: selectImage } : require('../asstes/Images/user_Two.png')}
                            />
                            <TouchableOpacity onPress={() => camer()}
                                style={{ position: 'absolute', bottom: 15, marginLeft: 65 }}>
                                <Image source={require('../asstes/Images/camera.png')}
                                    style={{ height: 35, width: 35, }}
                                />
                            </TouchableOpacity>
                        </View>
                        <TextInput placeholder="Jame Doe"
                            fontSize={20}
                            placeholderTextColor='black'
                            fontWeight='bold'
                            textAlign='center'
                            keyboardType="default"
                            onChangeText={(text) => { setSurname(text) }}
                            value={Surname}

                        />
                        <TextInput placeholder="janedoe123@email.com"
                            fontSize={12}
                            placeholderTextColor='black'
                            fontWeight='bold'
                            textAlign='center'
                            keyboardType="email-address"
                            onChangeText={(text)=>{ setEmail(text)}}
                            value={email}
                        />
                        <Text>Surname</Text>
                        <TextInput style={styles.input}
                            placeholder="Enter Surname"
                            keyboardType="default"
                            returnKeyType="next"
                        />
                        <Text>Name</Text>
                        <TextInput style={styles.input}
                            placeholder="Enter Name"
                            keyboardType="default"
                            returnKeyType="next"
                            onChangeText={(text) => { setName(text) }}
                            value={Name}
                        />
                        <Text>Country</Text>
                        <TextInput style={styles.input}
                            placeholder="Enter Country"
                            keyboardType="default"
                            returnKeyType="next"
                            onChangeText={(text) => { setCountry(text) }}
                            value={Country}
                        />
                        <Text>County</Text>
                        <TextInput style={styles.input}
                            placeholder="Enter County"
                            keyboardType="default"
                            returnKeyType="next"
                        />
                        <Text>Address</Text>
                        <TextInput style={styles.input}
                            placeholder="Enter Address"
                            keyboardType='email-address'
                            returnKeyType="next"
                        />
                        <Text>Zip Code</Text>
                        <TextInput style={styles.input}
                            placeholder="Enter Zip Code"
                            keyboardType='number-pad'
                            returnKeyType="next"
                        />
                        <Text>Phone Number</Text>
                        <TextInput style={styles.input}
                            placeholder="Enter Phone Number"
                            keyboardType='number-pad'
                            returnKeyType="next"
                        />
                        <TouchableOpacity onPress={() => onclick()}
                            style={styles.opacity}>
                            <Text style={styles.text}>Update Profile</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    image: {
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-between'
    },
    manimage: {
        height: 100,
        width: 100,
        alignSelf: 'center',
        marginVertical: 15,
        borderWidth: 2,
        borderColor: 'grey',
        borderRadius: 50, position: 'relative'
    },
    input: {
        backgroundColor: '#fff',
        borderWidth: 1,
        marginVertical: 10,
        borderRadius: 5,
        paddingHorizontal: 10
    },
    opacity: {
        backgroundColor: '#91bf3e',
        borderRadius: 20, marginVertical: 20
    },
    text: {
        color: '#fff',
        padding: 13,
        textAlign: 'center',
        fontWeight: 'bold'
    },
})
export default EditProfile;
