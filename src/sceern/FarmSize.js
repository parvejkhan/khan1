import React, { Component } from 'react';
import { View, Text, ImageBackground, TextInput, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import Tabs from '../navigastion/Tabs';

function FarmSize({ navigation }) {
    return (
        <View style={{ flex: 1, }}>
            <ImageBackground
                resizeMode='stretch'
                source={require('../asstes/Images/bg.png')}
                style={{ height: '100%', width: '100%', }}
            >
                <View style={{ height: 100 }}>
                    <Tabs currentPosition={4} />
                </View>
                <ScrollView style={{ paddingHorizontal: 20, marginVertical: 20 }}>
                    <Text style={{ fontSize: 12, color: '#91bf3e' }}>step4 of 5</Text>
                    <Text style={{ marginVertical: 30, fontSize: 20, fontWeight: 'bold' }}>Farm Size</Text>
                    <Text style={{ marginVertical: 7 }}>Farm Size</Text>
                    <TextInput style={{ borderWidth: 1, borderRadius: 10, paddingHorizontal: 10, backgroundColor: '#fff' }}
                        placeholder="Enter Phone Nubmer"
                        keyboardType="number-pad"

                    />
                    <Text style={{ fontSize: 10, marginVertical: 10 }}>Farm size in Acres ( 1/2 Acre, 1 Acre, 2 Acres, etc )</Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: "95%" }}>
                        <TouchableOpacity onPress={() => navigation.goBack()}
                            style={[styles.opcity, { backgroundColor: 'black' }]}>
                            <Text style={styles.next}>Back</Text>

                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate("FarmInfrastructure")}
                            style={styles.opcity}>
                            <Text style={styles.next}>Next</Text>

                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </ImageBackground>


        </View>
    );
}
const styles = StyleSheet.create({
    opcity: {
        backgroundColor: '#91bf3e',
        width: '35%',
        borderRadius: 25,
        elevation: 3,
        marginVertical: 10,
    },
    next: {
        padding: 15,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center',
        fontWeight: 'bold'
    },
})
export default FarmSize;
