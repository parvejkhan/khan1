import React, { Component, useState } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, TouchableOpacity, FlatList } from 'react-native';

function Account({ navigation, route }) {
    console.log('++++++++++++++',togglo)
    const [togglo, setTogglo] = useState(false)
    const onclick = (routes) => {
        setTogglo(!togglo)
        // navigation.navigate(routes)
        if (routes == 'dataCLear') {
            // AsyncStorage.clear()
            navigation.navigate('Login')
        } else {
            navigation.navigate(routes)
        }

    }
    const data = [
        { id: '1', image: require('../asstes/Images/Home.png'), text: 'Home', images: require('../asstes/Images/arrow-right.png'), routes: 'Home' },
        { id: '2', image: require('../asstes/Images/bell.png'), text: 'Notification', images: togglo  ? require('../asstes/Images/Switch-off.png') : require('../asstes/Images/Switch-on.png') },
        { id: '3', image: require('../asstes/Images/password.png'), text: 'Change Password', images: require('../asstes/Images/arrow-right.png'),routes:'ChangePassword' },
        { id: '4', image: require('../asstes/Images/logout.png'), text: 'Logout', images: require('../asstes/Images/arrow-right.png'),routes:'dataCLear' },
    ]

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                resizeMode='stretch'
                style={{ flex: 1 }}
                source={require('../asstes/Images/bg.png')}
            >
                <View style={{ paddingHorizontal: 20 }}>
                    <View style={styles.image}>
                        <TouchableOpacity onPress={() => navigation.goBack()}
                        >
                            <Image
                                source={require('../asstes/Images/back_arrow.png')}
                                style={{ height: 30, width: 30, resizeMode: "center" }}
                            />
                        </TouchableOpacity >
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>My  Account</Text>
                        <Text></Text>
                    </View>
                    <Image style={styles.manimage}
                        source={route.params && route.params.selectImage ? { uri: route.params.selectImage } : require('../asstes/Images/user_Two.png')}
                    />
                    <View style={{ alignSelf: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{route.params ? route.params.Surname : 'Name'}</Text>
                        <Text style={{ textAlign: 'center' }}>{route.params ? route.params.Email : 'email'}</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('EditProfile')}
                            style={{ alignSelf: 'center', borderWidth: 1, borderRadius: 20, borderColor: '#91bf3e', marginVertical: 10 }}>
                            <Text style={{ padding: 7, color: '#91bf3e', paddingHorizontal: 25 }}>Edit Profile</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 30 }}>
                        <FlatList
                            data={data}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => (
                                <View style={styles.lest}>
                                    <View style={styles.row}>
                                        <Image source={item.image}
                                            style={styles.imagelast}
                                        />
                                        <Text style={styles.text}>{item.text}</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => onclick(item.routes)}>
                                        <Image source={item.images}
                                            style={styles.imagelast}
                                        />
                                    </TouchableOpacity>
                                </View>
                            )}
                        />
                    </View>
                </View>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    image: {
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-between'
    },
    manimage: {
        height: 70,
        width: 70,
        alignSelf: 'center',
        marginVertical: 15,
        borderWidth: 2,
        borderColor: 'grey',
        borderRadius: 35
    },
    lest: {
        flexDirection: 'row',
        backgroundColor: '#e1eed2',
        padding: 10,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 5, borderRadius: 10,
    },
    imagelast: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
    },
    text: {
        marginLeft: 10,
        fontSize: 16
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
})
export default Account;
