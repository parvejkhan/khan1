import React, { useState, useRef } from 'react';
import { View, Text, ImageBackground, StyleSheet, Image, TouchableOpacity, } from 'react-native';

function Device() {

    return (
        <View style={{ flex: 1, }}>
            <ImageBackground
                resizeMode="stretch"
                source={require('../asstes/Images/bg_Two.png')}
                style={styles.baci}
            >
                <View style={{
                    height: 480, justifyContent: 'center',
                    alignItems: 'center',

                }}>
                    <Image
                        source={require('../asstes/Images/device_check_one.png')}
                        style={[styles.image,]}
                    />
                </View>
                <View style={styles.textview}>
                    <Text style={styles.text}>Device Found</Text>
                </View>
                <TouchableOpacity
                    style={styles.opacity}
                >
                    <Text style={styles.buttontext}>Cancel</Text>
                </TouchableOpacity>

            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    baci: {
        flex: 1,
        alignItems: 'center'
    },
    image: {
        height: 200,
        width: 200
    },
    textview: {
        marginBottom: 50
    },
    text: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    opacity: {
        backgroundColor: 'black',
        width: 300,
        borderRadius: 30,
        marginVertical: 40
    },
    buttontext: {
        color: '#fff',
        padding: 15,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    box: {
        backgroundColor: "#61dafb",
        width: 80,
        height: 80,
        borderRadius: 4,
    },
})
export default Device;
