import React, { useState } from 'react';
import { View, Text, ImageBackground, FlatList, StyleSheet, TouchableOpacity, TextInput, ScrollView, Button } from 'react-native';
import Tabs from '../navigastion/Tabs';

function Tayp({ navigation }) {
    const [data, setData] = useState([
        { id: '1', text: 'Dairy Farm', bgColor: 'red' },
        { id: '2', text: 'Beef Farm' },
        { id: '3', text: 'Fodder Farm' },
        { id: '4', text: 'Crop' },
        { id: '5', text: 'Specify the type of crop' },
        { id: '6', text: "Feedlots " },
        { id: '7', text: 'Feedlots' },
        { id: '8', text: 'Holdingground forgrazing' },
        { id: '9', text: 'Conservancy' },
        { id: '10', text: "Other" }

    ])
    const [isShowSelec, setIspress] = useState([])
    const button = (index) => {
        let teamData = [...data]
        teamData[index].isSelcd = !teamData[index].isSelcd
        setIspress({ teamData })
    }

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                resizeMode='stretch'
                source={require('../asstes/Images/bg.png')}
                style={{ height: '100%', width: '100%', }}
            >
                <View style={{ height: 100 }}>
                    <Tabs currentPosition={3} />
                </View>
                <ScrollView>
                    <View style={{ paddingHorizontal: 10, marginVertical: 10 }}>
                        <Text style={{ fontSize: 12, color: '#91bf3e' }}>step3 of 5</Text>
                        <Text style={{ marginVertical: 30, fontSize: 20, fontWeight: 'bold' }}>Type of Farm</Text>
                        <FlatList
                            data={data}
                            numColumns={2}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => (
                                <View style={styles.Lastitme}>
                                    <TouchableOpacity
                                        onPress={() => { button(index) }}
                                        style={[styles.opcity, {
                                            borderColor: item.isSelcd ? '#91bf3e' : "black",
                                            backgroundColor: item.isSelcd ? '#91bf3e' : "white",
                                        }]}>
                                        <Text style={[styles.next, { color: item.isSelcd ? 'white' : "black" }]}>{item.text}</Text>
                                    </TouchableOpacity>

                                </View>

                            )}
                        />

                        <TextInput style={{ borderWidth: 1, borderRadius: 10, height: 70 }}
                            placeholder="Enter Other"

                        />
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 30 }}>
                            <TouchableOpacity onPress={() => navigation.goBack()}
                                style={[styles.opcitys, { backgroundColor: 'black' }]}>
                                <Text style={styles.nexts}>Back</Text>

                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate("FarmSize")}
                                style={styles.opcitys}>
                                <Text style={styles.nexts}>Next</Text>

                            </TouchableOpacity>

                        </View>

                    </View>
                </ScrollView>
            </ImageBackground>

        </View>
    );
}
const styles = StyleSheet.create({
    Lastitme: {
        marginVertical: 10,
        flexDirection: 'row',
        // width:'45%'
    },
    opcity: {
        backgroundColor: '#fff',
        borderRadius: 25,
        borderWidth: 1,
        elevation: 3,
        marginVertical: 10,
        // width:'60%',
        marginLeft: 3
    },
    next: {
        padding: 10,
        // color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center',
        fontWeight: 'bold',
        paddingHorizontal: 25
    },
    opcitys: {
        backgroundColor: '#91bf3e',
        width: '35%',
        borderRadius: 25,
        elevation: 3,
        marginVertical: 10,
    },
    nexts: {
        padding: 15,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center',
        fontWeight: 'bold'
    },
})
export default Tayp;
