import React, { useState } from 'react';
import { View, Text, ImageBackground, Image, StyleSheet, TouchableOpacity, TextInput,  ScrollView, Modal } from 'react-native';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-crop-picker';
import Toptabs2 from '../navigastion/Toptabs2';
import {Picker} from '@react-native-picker/picker';



function FarmerRegistration({ navigation }) {
    const [selectedValue, setSelectedValue] = useState("java");
    const [date, setDate] = useState('09-10-2020');
    const [modalVisible, setModalVisible] = useState(false);

    const [selectCamer, setSelectCamer] = useState()
    const onclickcamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            setSelectImage(image.path)
            console.log(image);
        });
    }
    const [selectImage, setSelectImage] = useState()
    const onclick = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            setSelectImage(image.path)
            console.log(image);
        });

    }
    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                resizeMode="stretch"
                source={require('../asstes/Images/bg.png')}
                style={styles.bacg}
            >

                <ScrollView>
                    <Toptabs2 />
                    <View style={{ paddingHorizontal: 20 }}>
                        <View style={styles.padding}>
                            <Text style={{ fontSize: 12, color: '#91bf3e', marginTop: 20 }}>step1 of 2</Text>
                            <View style={{ position: 'relative', marginTop: -30 }}>
                                <Image source={selectImage ? { uri: selectImage } : require('../asstes/Images/user_Two.png')}
                                    style={styles.image}
                                />
                                <TouchableOpacity style={{ position: 'absolute', bottom: 0, alignSelf: 'flex-end' }}
                                    onPress={() => setModalVisible(true)}
                                >
                                    <Image source={require('../asstes/Images/camera.png')}
                                        style={styles.camera}
                                    />
                                </TouchableOpacity>
                            </View>
                            <Text></Text>
                        </View>
                        <Text style={{ marginVertical: 20, fontSize: 20, fontWeight: 'bold' }}>Farmer Registration</Text>
                        <Text style={{ fontSize: 16, fontWeight: 'bold', marginVertical: 10 }}>Farmer Bio-Data</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey' }}>Farmer's Name</Text>
                        <TextInput style={styles.Textinput}
                            placeholder="First Name"
                            keyboardType="default"
                        />
                        <TextInput style={styles.Textinput}
                            placeholder="Last Name"
                            keyboardType="default"
                        />
                        <TextInput style={styles.Textinput}
                            placeholder="Other Names"
                            keyboardType="default"
                        />
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey', marginVertical: 7 }}>Gender</Text>
                        <View style={{ borderWidth: 1, borderColor: 'grey', borderRadius: 5 }}>
                            <Picker selectedValue={selectedValue}
                                style={{ height: 50, width: 300, borderWidth: 1 }}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="setect Country" value="java" />
                                <Picker.Item label="hfbsh" value="js" />
                            </Picker>
                        </View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey', marginVertical: 7 }}>DOB</Text>
                        <View style={{ borderColor: 'grey', borderRadius: 5 }}>
                            <DatePicker
                                style={styles.datePickerStyle}
                                date={date} // Initial date from state
                                mode="date" // The enum of date, datetime and time
                                placeholder="select date"
                                format="DD-MM-YYYY"
                                minDate="01-01-2016"
                                maxDate="01-01-2019"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        //display: 'none',
                                        position: 'absolute',
                                        right: 0,
                                    },
                                    dateInput: {
                                        marginRight: 0,
                                    },
                                }}
                                onDateChange={(date) => {
                                    setDate(date);
                                }}
                            />

                        </View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey' }}>Phone Number</Text>
                        <TextInput style={styles.Textinput}
                            placeholder="Enter Phone Number "
                            keyboardType='number-pad'
                        />
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey' }}>Alternative Phone Number</Text>
                        <TextInput style={styles.Textinput}
                            placeholder="Enter Alternative Phone Number "
                            keyboardType='number-pad'
                        />

                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey' }}>Postal Address</Text>
                        <TextInput style={[styles.Textinput, { height: 100 }]}
                            placeholder="Enter Address "
                            keyboardType='number-pad'

                        />
                        <TouchableOpacity onPress={() => navigation.navigate("PaymentDetails")}
                            style={styles.opcity}>
                            <Text style={styles.next}>Next</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>

            </ImageBackground>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                       
                        <TouchableOpacity style={{ flexDirection: 'row', }}
                            onPress={() => onclickcamera()}
                        >
                            <Image style={{ height: 30, width: 30, resizeMode: 'contain' }}
                                source={require('../asstes/Images/camera-2.png')}
                            />
                            <Text style={styles.modalText}>Camera</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flexDirection: 'row', marginVertical: 20 }}
                            onPress={() => onclick()}
                        >
                            <Image style={{ height: 30, width: 30, resizeMode: 'contain' }}
                                source={require('../asstes/Images/gallery.png')}
                            />
                            <Text style={styles.modalText}>Gallery </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ marginVertical: 10 }}
                            onPress={() => setModalVisible(!modalVisible)}
                        >
                            <Image source={require('../asstes/Images/close-circle-outline.png')}
                                style={{ height: 20, width: 20 }}
                            />
                        </TouchableOpacity>

                    </View>

                </View>
            </Modal>

        </View>
    );
}
const styles = StyleSheet.create({
    bacg: {
        height: '100%',
        width: '100%'
    },
    padding: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    image: {
        height: 100,
        width: 100,
        borderWidth: 2,
        borderColor: 'grey',
        borderRadius: 50
    },
    camera: {
        height: 30,
        width: 30,

    },
    Textinput: {
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        marginVertical: 5
    },
    datePickerStyle: {
        width: 330,
        marginTop: 5,
        padding: 2,

    },
    opcity: {
        backgroundColor: '#91bf3e',
        width: '30%',
        marginVertical: 40,
        borderRadius: 20,
        alignSelf: 'flex-end',
        elevation: 3
    },
    next: {
        padding: 10,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center'
    },



    centeredView: {
         justifyContent:'center',  
        alignItems: 'center',
        marginTop: '20%',
        flex: 1,
        backgroundColor:'#00000090'
    },
    modalView: {

        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        elevation: 1
    },
    modalText:{
        marginLeft:15
    }


})
export default FarmerRegistration;
