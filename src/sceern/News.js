import React ,{useEffect, useState} from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Image, StyleSheet, TextInput, FlatList, ScrollView } from 'react-native';
import Slider from "react-native-hook-image-slider"
import { ApiUrl } from './Config/urls';
import * as axios from 'axios'



function News({ navigation, route }) {
    useEffect(() => {
        productListData()

    }, [])
    const [productList, setProductList] = useState([])
    

    const productListData = async () => {
        // await axios.get(ApiUrl.baseUrl+ApiUrl.product).then((resp => {
        //     var result = resp.data.products
        //     // alert('ckljn')
        //     console.log('resp', resp.data.data)
        //     setProductList(result)
        // }))

        await axios.get('https://api.princeex.com/frontapi/Coin_Pair_Data').then((resp => {
            var result = resp.data.data
            // console.log('resp', resp.data)
            setProductList(result)
        }))

    }
    
    console.log('++++++++++++++', route.params ? route.params.title : '')
    const [data ,setData] = useState  ([
        {
            id: '1', image: require('../asstes/Images/images.jpeg'), text: 'farmers life', date: 'june 2, 2002',
            lasi: 'Lorem Ipsum is simply dummy text ofprinting and typesetting industry.'
        },
        {
            id: '2', image: require('../asstes/Images/images.jpeg'), text: 'farmers life', date: 'june 2, 2002',
            lasi: 'Lorem Ipsum is simply dummy text ofprinting and typesetting industry.'
        },
        {
            id: '3', image: require('../asstes/Images/images.jpeg'), text: 'farmers life', date: 'june 2, 2002',
            lasi: 'Lorem Ipsum is simply dummy text ofprinting and typesetting industry.'
        },
        {
            id: '4', image: require('../asstes/Images/images.jpeg'), text: 'farmers life', date: 'june 2, 2002',
            lasi: 'Lorem Ipsum is simply dummy text ofprinting and typesetting industry.'
        },
        {
            id: '5', image: require('../asstes/Images/images.jpeg'), text: 'farmers life', date: 'june 2, 2002',
            lasi: 'Lorem Ipsum is simply dummy text ofprinting and typesetting industry.'
        },
    ])
    
    const  _renderItem =(data)=>{
        return data.map((item,index)=>{
            return(
                <View key={index} style={styles.itemlast}>
                <Image source= {{uri: 'https://api.princeex.com/static/currencyImage/' +  item.coinIcon}}
                    style={styles.images}
                    // uri: 'https://api.princeex.com/static/currencyImage/' + item.image 
                />
                <View>
                    <View style={styles.manview}>
                        <TouchableOpacity onPress={() => navigation.navigate("Dateil" ,item)}
                            style={styles.opcity}>
                            <Text style={styles.list}>{item.coin_first_name}</Text>
                        </TouchableOpacity>
                        <Text style={styles.date}>{item.coin_Second_name}</Text>
                    </View>
                       <View style={{ width: 240 }}>
                       <Text style={styles.pages}>{item.changePricePercent} %</Text>
                    </View>
                   
                </View>
             
            </View>
            )

        })

    }


    return (
        <View style={{ flex: 1 }}>
            <ImageBackground
                resizeMode="stretch"
                source={require('../asstes/Images/bg_Two.png')}
                style={{ flex: 1 }}
            >

                <View style={{ paddingHorizontal: 20 }}>
                    <View style={styles.image}>
                        <TouchableOpacity onPress={() => navigation.goBack()}
                        >
                            <Image
                                source={require('../asstes/Images/back_arrow.png')}
                                style={{ height: 30, width: 30, resizeMode: "center" }}
                            />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>News</Text>
                        <TouchableOpacity onPress={() => navigation.navigate("AddNews")}
                        >
                            <Image
                                source={require('../asstes/Images/add-post.png')}
                                style={{ height: 30, width: 30, resizeMode: "center" }}
                            />
                        </TouchableOpacity>
                    </View>
                   
                        <View style={{ backgroundColor: '#dfdfdf', marginVertical: 10, flexDirection: 'row', alignItems: 'center', height: 40, borderRadius: 10 }}>
                            <Image source={require('../asstes/Images/search.png')}
                                style={{ height: 20, width: 20, marginLeft: 10 }}
                            />
                            <TextInput style={{ marginLeft: 10, flex: 1, }}
                                placeholder="Search"
                            />
                        </View>
                        <ScrollView style={{marginBottom:150}} showsVerticalScrollIndicator={false}>

                        <View style={{ marginVertical: 20, }}>
                            <Slider
                                images=
                                {[
                                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPwPhiqhGsm0GHEM3aRckqAZsWhuBwHSwXQg&usqp=CAU",
                                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfeX5CHFufwq1Y3DdakcBHByd_c3NTXbLhJg&usqp=CAU",
                                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtclqir0QdsnBnyhruyjfga729j2NeDNjBew&usqp=CAU",
                                ]}
                            />
                        </View>
                        {/* <Text style={{}}>{route.params.title}</Text> */}
                        {_renderItem (productList || [])}

                        </ScrollView>
                     
                </View>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    image: {
        flexDirection: 'row',
        marginTop: 40,
        justifyContent: 'space-between'
    },
    itemlast: {
        flexDirection: 'row',
        backgroundColor: '#dfdfdf',
        borderRadius: 10,
        padding:15,
        marginVertical: 10,
        elevation: 1
    },
    images: {
        height: 60,
        width: 60,
        borderRadius: 10,
        resizeMode:'center'
    },
    list: {
        color: '#fff',
        padding: 5,

    },
    manview: {
        flexDirection: 'row',
        marginLeft: 10,
        marginVertical: 5,
        width: 220
    },
    opcity: {
        backgroundColor: '#91bf3e',
        height: 30,
        borderRadius: 10
    },
    date: {
        marginLeft: 30,
        color: 'grey'
    },
    pages: {
        marginLeft: 10,
        fontSize: 12,
    },
})
export default News;
