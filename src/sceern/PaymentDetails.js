import React, { useState } from 'react';
import { View, Text, ImageBackground, StyleSheet, TextInput,TouchableOpacity,Image, ScrollView } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Toptabs2 from '../navigastion/Toptabs2';
import {Picker} from '@react-native-picker/picker';


function PaymentDetails({navigations}) {
    const [selectedValue, setSelectedValue] = useState("java");
    const [selectImage, setSelectImage] = useState("");
    const button = () => {
        ImagePicker.openPicker({
              width: 100,
              height: 100,
              borderRadius: 100 / 2,
              cropping:true
          }).then(image => {
              setSelectImage(image.path)
              console.log(image);
          });
  
    }
    const[selectCamera,setSelectCamera]=useState('')
    const Camera=()=>{
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true,
          }).then(image => {
            setSelectCamera(image.path)
            console.log(image);
          });
          
    }
    return (
        <View>
            <ImageBackground
                resizeMode='stretch'
                source={require('../asstes/Images/bg.png')}
                style={styles.bacgi}
            >
                <Toptabs2 />
                <ScrollView>
                <View style={{ paddingHorizontal: 20 }}>
                    <Text style={{ fontSize: 12, color: '#91bf3e', marginTop: 20 }}>step1 of 2</Text>
                    <Text style={{ marginVertical: 20, fontSize: 20, fontWeight: 'bold' }}>Farmer's Payment Details</Text>
                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey' }}>Farmer's id Number</Text>
                    <TextInput style={styles.Textinput}
                        placeholder="Enter id Number"
                        keyboardType='number-pad'
                    />
                    <Text style={{ fontSize: 14,marginVertical:10, fontWeight: 'bold', color: 'grey' }}>Farmer's ID Photo</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View>
                        <View style={styles.image}>
                            <TouchableOpacity onPress={() => button()}
                            >
                                <Image source={selectImage? { uri: selectImage} : require('../asstes/Images/plus.png')}
                                    style={{ height: 70, width: 70 }}
                                />
                               
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.text}>Front</Text>
                        </View>
                        <View>
                        <View style={[styles.image,{marginLeft:10}]}>
                            <TouchableOpacity onPress={()=>Camera() }
                            >
                            <Image source={selectCamera ? { uri: selectCamera } : require('../asstes/Images/plus.png')}
                                style={{height:70,width:70}}
                            />
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.text}>Back</Text>
                        </View>

                    </View>
                    <Text style={{ fontSize: 14,marginVertical:10, fontWeight: 'bold', color: 'grey' }}>Photo of ATM card (Optional)</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View>
                        <View style={styles.image}>
                            <TouchableOpacity onPress={() => button()}
                            >
                                <Image source={selectImage? { uri: selectImage} : require('../asstes/Images/plus.png')}
                                    style={{ height: 70, width: 70 }}
                                />
                               
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.text}>Front</Text>
                        </View>
                        <View>
                        <View style={[styles.image,{marginLeft:10}]}>
                            <TouchableOpacity onPress={()=>Camera() }
                            >
                            <Image source={selectCamera ? { uri: selectCamera } : require('../asstes/Images/plus.png')}
                                style={{height:70,width:70}}
                            />
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.text}>Back</Text>
                        </View>

                    </View>
                    <Text style={{ fontSize: 14,marginVertical:10, fontWeight: 'bold', color: 'grey' }}>Bank Name</Text>
                    <View style={styles.Textinput}>
                    <Picker selectedValue={selectedValue}
                            style={{ height: 50, width: 300 }}
                            onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                        >
                            <Picker.Item label="setect Country" value="java" />
                            <Picker.Item label="SBI" value="js" />
                            <Picker.Item label="ICICI" value="js"/>

                        </Picker>
                        </View>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey' }}>Bank Branch</Text>
                    <TextInput style={styles.Textinput}
                        placeholder="Enter Branch Name"
                        keyboardType='number-pad'
                    />
                     <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'grey' }}>Account Number</Text>
                    <TextInput style={styles.Textinput}
                        placeholder="Enter Account Number"
                        keyboardType='number-pad'
                    />
                      <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginVertical:30}}>
                        <TouchableOpacity onPress={()=>navigation.goBack()}
                        style={[styles.opcitys,{backgroundColor:'black'}]}>
                            <Text style={styles.nexts}>Back</Text>

                        </TouchableOpacity>
                        <TouchableOpacity 
                        style={styles.opcitys}>
                            <Text style={styles.nexts}>Finish</Text>

                        </TouchableOpacity>

                    </View>






                </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    bacgi: {
        height: '100%',
        width: '100%'
    },
    Textinput: {
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        marginVertical: 10
    },
    image: {
        borderWidth: 1,
        height: 100,
        width: 90,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text:{
        textAlign:'center',
        fontSize:16,
        color:'grey'
    },
    opcitys: {
        backgroundColor: '#91bf3e',
        width: '35%',
        borderRadius: 25,
        elevation: 3,
        marginVertical:10,
    },
    nexts: {
        padding: 15,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center',
        fontWeight:'bold'
    },
})
export default PaymentDetails;
