import React, { useState } from 'react';
import { View, Text, ImageBackground, StyleSheet, FlatList, TouchableOpacity, TextInput, ScrollView, Button, Image } from 'react-native';
import Tabs from '../navigastion/Tabs';

function FarmInfrastructure({ navigation }) {
    const [data, setData] = useState([
        { id: '1', text: 'Weighing Machine' },
        { id: '2', text: 'Feed Cutting' },
        { id: '3', text: 'Mixing Machine' },
        { id: '4', text: 'Water Pump' },
        { id: '5', text: 'Other' },
    ])
    const [datas, setdatas] = useState([
        { id: '1', texts: 'Borehole' },
        { id: '2', texts: 'Piped water' },
        { id: '3', texts: 'River' },
        { id: '4', texts: 'Pumping from a nearby river' },
        { id: '5', texts: 'Other' },
    ])

    const [isShowSelec, setIspress] = useState([])
    const onclick = (index) => {
        let teamData = [...data]
        teamData[index].isSelcd = !teamData[index].isSelcd
        setIspress({ teamData })
    }
    const [hinnd, setHinnd] = useState([])
    const button = (index) => {
        let arreydata = [...datas]
        arreydata[index].isSeleat = !arreydata[index].isSeleat
        setHinnd([arreydata])
    }


    return (
        <View style={{ flex: 1, }}>
            <ImageBackground
                resizeMode='stretch'
                source={require('../asstes/Images/bg.png')}
                style={{ height: '100%', width: '100%', }}
            >
                <View style={{ height: 100 }}>
                    <Tabs currentPosition={4} />
                </View>
                <ScrollView>
                    <View style={{ paddingHorizontal: 20, marginVertical: 10 }}>
                        <Text style={{ fontSize: 12, color: '#91bf3e' }}>step5 of 5</Text>
                        <Text style={{ marginVertical: 30, fontSize: 20, fontWeight: 'bold' }}>Farm Infrastructure</Text>
                        <Text style={{ marginVertical: 10, fontSize: 16, fontWeight: 'bold' }}>Machinery</Text>
                        <FlatList
                            data={data}
                            numColumns={2}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => (
                                <View style={styles.Lastitme}>
                                    <TouchableOpacity onPress={() => { onclick(index) }}
                                        style={[styles.opcity, {
                                            borderColor: item.isSelcd ? '#91bf3e' : "black",
                                            backgroundColor: item.isSelcd ? '#91bf3e' : '#fff'
                                        }]}>
                                        <Text style={[styles.next, {
                                            color: item.isSelcd ? '#fff' : 'black'
                                        }]}>{item.text}</Text>
                                        <Image  source={require('../asstes/Images/call.png') }/>
                                    </TouchableOpacity>
                                </View>
                            )}
                        />
                        <Text style={{ marginVertical: 10, fontSize: 16, fontWeight: 'bold' }}>Water Sources</Text>

                        <FlatList
                            data={datas}
                            numColumns={3}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => (
                                <View style={styles.Lastitme1}>
                                    <TouchableOpacity onPress={() => { button(index) }}
                                        style={[styles.opcity, {
                                            backgroundColor: item.isSeleat ? "#91bf3e" : '#fff',
                                            borderColor: item.isSeleat ? "#91bf3e" : 'black'
                                        }
                                        ]}>
                                        <Text style={[styles.next, {
                                            color: item.isSeleat ? '#fff' : 'black'
                                        }
                                        ]}>{item.texts}</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                        />
                        <TextInput style={{ borderWidth: .5, height: 90, borderRadius: 10, marginVertical: 10 }} />
                        <Text style={{ marginVertical: 10, fontSize: 16, fontWeight: 'bold' }}>Storage Capacity</Text>
                        <FlatList
                            data={data}
                            numColumns={2}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => (
                                <View style={styles.Lastitme}>
                                    <TouchableOpacity onPress={() => { onclick(index) }}
                                        style={[styles.opcity, {
                                            borderColor: item.isSelcd ? '#91bf3e' : "black",
                                            backgroundColor: item.isSelcd ? '#91bf3e' : '#fff'
                                        }]}>
                                        <Text style={[styles.next, {
                                            color: item.isSelcd ? '#fff' : 'black'
                                        }]}>{item.text}</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                        />
                        <Text style={{ marginVertical: 10, fontSize: 16, fontWeight: 'bold' }}>Power Availability</Text>
                        <FlatList
                            data={data}
                            numColumns={2}
                            keyExtractor={item => item.id}
                            renderItem={({ item, index }) => (
                                <View style={styles.Lastitme}>
                                    <TouchableOpacity onPress={() => { onclick(index) }}
                                        style={[styles.opcity, {
                                            borderColor: item.isSelcd ? '#91bf3e' : "black",
                                            backgroundColor: item.isSelcd ? '#91bf3e' : '#fff'
                                        }]}>
                                        <Text style={[styles.next, {
                                            color: item.isSelcd ? '#fff' : 'black'
                                        }]}>{item.text}</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                        />
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 30 }}>
                            <TouchableOpacity onPress={() => navigation.goBack()}
                                style={[styles.opcitys, { backgroundColor: 'black' }]}>
                                <Text style={styles.nexts}>Back</Text>

                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.opcitys}>
                                <Text style={styles.nexts}>Finish</Text>

                            </TouchableOpacity>

                        </View>
                    </View>
                </ScrollView>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    Lastitme: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: "50%"

    },
    Lastitme1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 10

    },
    opcity: {
        backgroundColor: '#fff',
        borderRadius: 25,
        elevation: 3,
        marginVertical: 10,
        borderWidth: 1
    },
    next: {
        padding: 10,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingHorizontal: 20,
        fontSize: 12
    },
    opcitys: {
        backgroundColor: '#91bf3e',
        width: '35%',
        borderRadius: 25,
        elevation: 3,
        marginVertical: 10,
    },
    nexts: {
        padding: 15,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center',
        fontWeight: 'bold'
    },

})
export default FarmInfrastructure;
