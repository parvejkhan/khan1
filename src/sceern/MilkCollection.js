import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, TouchableOpacity, Image, StyleSheet, TextInput, FlatList, ScrollView, } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import axios from 'axios'
function MilkCollection(props) {
    const { refresh } = props
    const [products, setProducts] = useState('')
    useEffect(() => {
        fetch('https://bitglobal.bngvogue.com/opencart/index.php?route=api/product')
            .then((response) => response.json())
            .then((json) => setProducts(json.products))
            .catch(err => {
                console.log('jfghher', products)
            })
            console.log('jfghher', products)

    }, [])
    const [searchString, setSearchString] = useState("");
    console.log('searchString', searchString)
    const _filter = (item, search) => {
        if ((item.name).toLowerCase().search(search.toLowerCase()) !== -1 || (item.price).toLowerCase().search(search.toLowerCase()) !== -1) {
            return item
        }
    }

    // const products = [
    //     { id: '1', image: require('../asstes/Images/user_one.png'), text: 'Sal Monella', row: '2 Junction Mall, Dagoretti', number: '001', routes: 'SalMonella' },
    //     { id: '2', image: require('../asstes/Images/user_Two.png'), text: 'Sal Monella', row: '2 Junction Mall, Dagoretti', number: '002' },
    //     { id: '3', image: require('../asstes/Images/user_one.png'), text: 'Sal Monella', row: '2 Junction Mall, Dagoretti', number: '003' },
    //     { id: '4', image: require('../asstes/Images/user_one.png'), text: 'Sal Monella', row: '2 Junction Mall, Dagoretti', number: '004' },
    //     { id: '5', image: require('../asstes/Images/user_one.png'), text: 'Sal Monella', row: '2 Junction Mall, Dagoretti', number: '005' },
    //     { id: '6', image: require('../asstes/Images/user_one.png'), text: 'Sal Monella', row: '2 Junction Mall, Dagoretti', number: '006' },
    //     { id: '7', image: require('../asstes/Images/user_one.png'), text: 'Sal Monella', row: '2 Junction Mall, Dagoretti', number: '007' },
    //     { id: '8', image: require('../asstes/Images/user_one.png'), text: 'Sal Monella', row: '2 Junction Mall, Dagoretti', number: '008' },
    // ]
    const onclick = (products) => {
        props.navigation.navigate('SalMonella', { products: products, backScreen: 'MilkCollection' })
    }

    const _renderItem = (products) => {
      
        return products.map((item, index) => {
            if (_filter(item, searchString))
            return (
                <TouchableOpacity key={index} onPress={() => onclick(item)}
                    style={styles.itemlist}>
                    <Image style={styles.image}
                        source={{ uri: item.thumb }}
                    />
                    <View style={styles.name}>
                        <Text style={{ fontSize: 20, }}>{item.name}</Text>
                        <Text>{item.description}</Text>

                    </View>
                    <View style={styles.conte}>
                        <Text style={styles.number}>{item.price}</Text>
                    </View>
                </TouchableOpacity>
            )

        })
    }



    return (
        <View style={{ flex: 1, }}>
            <ImageBackground
                resizeMode="stretch"
                source={require('../asstes/Images/bg_Two.png')}
                style={{ flex: 1, }}
            >
                <View style={{ paddingHorizontal: 15 }}>
                    <View style={styles.manview}>
                        <TouchableOpacity onPress={() => props.navigation.goBack()}>
                            <Image style={{ height: 30, width: 30, resizeMode: 'contain' }}
                                source={require('../asstes/Images/back_arrow.png')}
                            />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Milk Collection</Text>
                        <Text></Text>

                    </View>
                    <View style={styles.search}>
                        <Image style={{ height: 25, width: 25, resizeMode: 'contain' }}
                            source={require('../asstes/Images/search.png')}
                        />
                        <TextInput
                            placeholder="Search by Farmer No .Or Routs"
                            onChangeText={(e) => setSearchString(e)}
                        />
                    </View>
                </View>
                <KeyboardAwareScrollView style={{ paddingHorizontal: 15 }}>


                    <ScrollView>
                        {_renderItem(products || [])}
                    </ScrollView>

                </KeyboardAwareScrollView>
            </ImageBackground>
        </View>
    );
}
const styles = StyleSheet.create({
    manview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 20
    },
    search: {
        marginVertical: 10,
        backgroundColor: '#eeeef0',
        elevation: 2,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    itemlist: {
        backgroundColor: '#f8f8f8',
        elevation: 5,
        // height: 100,
        borderRadius: 10,
        // flexDirection: 'row',
        marginTop: 15
    },
    image: {
        height: 80,
        width: 80,
        marginLeft: 10,
        marginTop: 10
    },
    name: {
        marginLeft: 10,
        marginTop: 10
    },
    conte: {
        // backgroundColor: '#91bf3e',
        height: 30,
        marginTop: 10,
        borderRadius: 5
    },
    number: {
        padding: 4,
        // color: '#fff',
        fontWeight: 'bold'
    },

})
export default MilkCollection;
