export const ApiUrl = {
    
    baseUrl: 'https://bitglobal.bngvogue.com/opencart/index.php?route=api/product',
    login: 'login',
    register:'register',
    edit:'edit',
    contact:'contact',
    forgotten:'forgotten',
    password:'password',
    product:'product',
 
}

// export const productlistUrl={
//     baseUrl:'https://bitglobal.bngvogue.com/opencart/index.php?route=api/allproducts/categories&json',
//     allproducts:'allproducts'
// }