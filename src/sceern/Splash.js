import React, { useState,useEffect } from 'react';
import { View, Text, ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';


function Splash(props){
    // useEffect(() => {
    //     setTimeout(() => {
    //       props.navigation.navigate('Login', { backScreen: 'Splash' })
    //     }, 1000);
    //   }, []);
      useEffect(() => {
        setTimeout(() => {
          AsyncStorage.getItem('token').then((value) => {
            if (value !== null) {
              getUserVeried(value)
            } else {
              AsyncStorage.clear()
              props.navigation.navigate('Login', { backScreen: 'Splash' })
            }
          })
        }, 1000);
      }, []);

      // else {
      //   AsyncStorage.clear()
      // }
    return (
      <View style={{flex:1}}>
       <ImageBackground  
       resizeMode="stretch"
       source={require('../asstes/Images/splash.png')}
        style={{flex:1}}
       >

       </ImageBackground>
      </View>
    );
  }
export default Splash;
