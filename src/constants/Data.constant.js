import AsyncStorage from "@react-native-async-storage/async-storage"

// const mainUrl = 'https://api.masterx.live/'
const mainUrl = 'https://bitglobal.bngvogue.com/'

const base ={
    api: mainUrl +'opencart/',
    imageBaseUrl: mainUrl+'image/cache/catalog/data/',
    imageBaseUrlHelp: mainUrl+'static/helpIssue/',
    token:  AsyncStorage.getItem('token')
}
// const baseUrl = 'http://172.104.186.216:3004/'  https://bitglobal.bngvogue.com/opencart/image/cache/catalog/data/CST2601-228x228.jpg

export {
    mainUrl,
    base
    // baseUrl
}