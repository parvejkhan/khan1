import { StyleSheet, Platform } from 'react-native'
import { screenHeight, screenWidth } from './Sizes.constant'
import { ColorsConstant } from './Colors.constant'
const c = ColorsConstant
export const StyleConstants = StyleSheet.create({
    content: {
        height: screenHeight,
        width: screenWidth,
        backgroundColor: c.Dark
    },
    icon: {
        width: 20,
        height: 20,
        alignSelf: 'center'
    },
    loader: {
        position: 'absolute',
        alignSelf: 'center',
        width: screenWidth,
        height: screenHeight,
        zIndex: 9999,
        backgroundColor: c.DarkLight

    },
    Textinput: {
        borderWidth: 1,
        borderColor: ColorsConstant.BorderColor,
        borderRadius: 7,
        paddingHorizontal: 10,
        marginVertical: 15,
        width:screenWidth -40
    },
    textsigup: {
        textAlign: 'center',
        color: ColorsConstant.White,
        fontWeight: 'bold'
    },
    btnTheme: {
        backgroundColor: ColorsConstant.Btntheme,
        padding: 16,
        borderRadius: 10,
        marginVertical: 20,
        width: screenWidth - 45
    },
    securityCheck: {
        width: 35,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnOutline: {
        borderWidth: 1.5,
        padding: 16,
        borderRadius: 10,
        borderColor: ColorsConstant.BorderColor,
        width: screenWidth - 45
    },
    modalView:{ 
        flex: 1,
        backgroundColor:'gray',
        opacity:0.7,position:'absolute',top:0,left:0 ,right:0,bottom:0
    }
})