import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';


import Splash from '../sceern/Splash';
import Login from '../sceern/Login';
import Registra from '../sceern/Registra';
import Toptabs from './Toptabs';
import Ownership from '../sceern/Ownership';
import Tayp from '../sceern/Tayp';
import Home from '../sceern/Home';
import FarmSize from '../sceern/FarmSize';
import FarmInfrastructure from '../sceern/FarmInfrastructure';
import FarmerRegistration from '../sceern/FarmerRegistration';
import PaymentDetails from '../sceern/PaymentDetails';
import AddNews from '../sceern/AddNews';
import News from '../sceern/News';
import Dateil from '../sceern/Dateil';
import Account from '../sceern/Account';
import EditProfile from '../sceern/EditProfile';
import ChangePassword from '../sceern/ChangePassword';
import newpassword from '../sceern/newpassword';
import MilkCollection from '../sceern/MilkCollection';
import Device from '../sceern/Device';
import SalMonella from '../sceern/SalMonella';
import RegisterSIGN from '../sceern/RegisterSIGN';


const Stack = createNativeStackNavigator();

function Routes  () {
  return (
    <Stack.Navigator  screenOptions={{ headerShown: false }} initialRouteName="Home">
      <Stack.Screen name="Splash" component={Splash} Screen={Splash} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="RegisterSIGN" component={RegisterSIGN} />
      <Stack.Screen name="Home" component={Home}  Screen={Home} />
      <Stack.Screen name="Registra" component={Registra} />
      <Stack.Screen name="Toptabs" component={Toptabs} />
      <Stack.Screen name="Ownership" component={Ownership} />
      <Stack.Screen name="Tayp" component={Tayp} />
      <Stack.Screen name="FarmSize" component={FarmSize} />
      <Stack.Screen name="FarmInfrastructure" component={FarmInfrastructure} />
      <Stack.Screen name="FarmerRegistration" component={FarmerRegistration} />
      <Stack.Screen name="PaymentDetails" component={PaymentDetails} />
      <Stack.Screen name="News" component={News} />
      <Stack.Screen name="AddNews" component={AddNews} />
      <Stack.Screen name="Dateil" component={Dateil} />
      <Stack.Screen name="Account" component={Account} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="newpassword" component={newpassword} />
      <Stack.Screen name="MilkCollection" component={MilkCollection} />
      <Stack.Screen name="Device" component={Device} />
      <Stack.Screen name="SalMonella" component={SalMonella} />

    </Stack.Navigator>
  );
};
// const Drawer = createDrawerNavigator();

const MyTabs =() =>{
  return (
    <NavigationContainer>
    {/* <Drawer.Navigator
      drawerContent={RegisterSIGN}
      screenOptions={{
        headerShown: false,
        drawerType: 'front',
        swipeEnabled: false,
        drawerStyle: {
          width:300,
        },
      }}>
      <Drawer.Screen name="Routes" component={Routes} />
    </Drawer.Navigator> */}
    <Routes/>
  </NavigationContainer>

  )
}

export default MyTabs;