import React, { useState } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';

function Toptabs2(toggol) {
    return (
        <View>
            <View style={{
                flexDirection: 'row',
                alignSelf: 'center',
                justifyContent: 'space-between',
                width: '15%',
                marginTop: 20,
                height: 70
            }}>
                <TouchableOpacity onPress={() => onclick(toggol)}
                >
                    <Image source={toggol == false ? require('../asstes/Images/green-circle.png') : require('../asstes/Images/green-circle.png')}
                        style={{ height: 20, width: 20, }}
                    />
                </TouchableOpacity>
                <Image source={require('../asstes/Images/dot.png')}
                    style={{ height: 20, width: 20 }}
                />
                {/* <Image source={require('../asstes/Images/dot.png')}
                    style={{ height: 20, width: 20 }}
                />
                <Image source={require('../asstes/Images/dot.png')}
                    style={{ height: 20, width: 20 }}
                />
                <Image source={require('../asstes/Images/dot.png')}
                    style={{ height: 20, width: 20 }}
                /> */}
            </View>
            <View style={{ borderWidth: 1, width: '15%', alignSelf: 'center', position: 'absolute', bottom: 59, borderColor: 'grey' }}>
            </View>
        </View>
    );
}
export default Toptabs2;
