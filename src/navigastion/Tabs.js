



import React, { Component, useState } from 'react';
import { Text, View,StyleSheet } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
// import { ColorsConstant } from '../constants/Colors.constant';
const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize:30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#273671',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#273671',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: "#273671",
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#273671',
    stepIndicatorUnFinishedColor: 'white',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: 'black',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: 'gray',
    labelColor: '#999999',
    labelSize: 10,
    currentStepLabelColor: 'black'
  }

function Tabs(props) {
    const {currentPosition} = props
    const labels = ["Registrtion", "Ownership", "Verified Id", "Verified Address", "Verified Selfie",];
    return (
        <>
            <View style={{ flex: 1,marginTop:50 }}>
                <StepIndicator 
                    customStyles={customStyles}
                    currentPosition={currentPosition}
                    labels={labels}
                    // stepIndicatorLabelCurrentColor={'white'}
                    
                 
                    
                    // onPress={(e) => setcurrentPosition(e)}
                />
            </View>
        </>
    )

}
const styles = StyleSheet.create({
    manview: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 30,
    },
    customStyles:{
        color: "#fff",
    },
    image: {
        height: 25,
        width: 25,
    },
    textkyc: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'absolute', bottom: -45
    },
    list: {
        alignItems: 'center',
        width:40,
        height: 40,
        justifyContent: 'center'
    },
    active: {
        backgroundColor: '#b8860b',
        borderRadius: 40
    },
    textemail: {
        color: '#fff',
        fontSize: 8,
        marginVertical: 10
    },
})
export default Tabs;
