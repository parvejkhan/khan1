import React, { useEffect } from 'react'
import { StyleSheet, Text, ActivityIndicator, TouchableOpacity, View, BackHandler, Alert, Image } from 'react-native'
import { Appbar } from 'react-native-paper';
import { screenWidth } from '../constants/Sizes.constant';
import { ColorsConstant } from '../constants/Colors.constant';

export default function Header(props) {
    const { refresh, setRefresh, animating, setAnimating } = props

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", _goBack);
        return (() => {
            BackHandler.removeEventListener("hardwareBackPress", _goBack)
        })
    }, [refresh])

    const _goBack = () => {
        // if (typeof props.leftButtonAction === "undefined") {
        //     Alert.alert("Hold on!", "Are you sure you want to exit the app?",
        //         [{
        //             text: "Cancel",
        //             onPress: () => null,
        //             style: "cancel"
        //         },
        //         { text: "YES", onPress: () => BackHandler.exitApp() }
        //         ])
        //     return true
        // } else {
        //     props.leftButtonAction()
        //     // return true
        // }
    }
    const _handleRefresh = async () => {
        const { user } = props;
        // let result = await getUserPlan(user.token);
        // let userResult = await getUser(user.token);
        if (result.status) {

        }
        if (userResult.status) {
            let userData = userResult.data;
            let refreshedData = { ...user, ...userData }
            //console.log('refreshedData',refreshedData)
        }
    }
    const refreshData = () => {
        setRefresh(new Date().getTime())
    }
    return (<>
        <Appbar.Header style={{ backgroundColor: ColorsConstant.theme, position: 'relative', zIndex: 1 }}>
            {
                props.leftButttonType !== 'noIcon' ?
                    <Appbar.Action style={{ width: typeof props.leftButtonType == 'undefined' ? 50 : 40, height: typeof props.leftButtonType == 'undefined' ? 50 : 40, borderRadius: 4, backgroundColor: props.buttonBackColor, alignItems: 'center' }} icon={() =>
                        props.leftButtonType === 'back' ?
                            <TouchableOpacity style={ls.leftButton} 
                            //  onPress={_goBack}
                             >
                                {/* <Text>hghj</Text> */}

                                <Image source={require('../asstes/Images/back-arrow-white.png')} style={ls.leftButtonIcon} />
                            </TouchableOpacity> :
                            props.leftButtonTemplate}
                    /> : <Text></Text>
            }
            <Appbar.Content title={props.title} />
            {
                props.rightButttonType === 'refresh' && props.rightButtonAction !== 'undefined' ?
                    <View>
                        {props.rightButtonAction}
                        {/* <TouchableOpacity style={{ width: 40, height: 40, }} onPress={() => AsyncStorage.clear()}>
                            <Text style={{ color: 'red' }}>OO</Text>
                        </TouchableOpacity> */}
                        </View> : <Text></Text>
            }
        </Appbar.Header>
        {animating && <ActivityIndicator
            animating={animating}
            color="red"
            size="small"
            style={ls.activityIndicator}
        />}
    </>)
}
const ls = StyleSheet.create({
    headerContainer: {
        height: 56,
        paddingTop: 30,
        zIndex: 10,
        paddingHorizontal: 5,
    },
    activityIndicator: {
        backgroundColor: '#0000004f',
        alignItems: 'center',
        justifyContent: "center",
        position: 'absolute',
        zIndex: 9999,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    isModalHeader: {
        height: 50
    },
    rightButtonIcon: {
        tintColor: ColorsConstant.White,
        width: 25,
        height: 25,
    },
    safeHeader: {
        height: 24
    },
    headerBar: {
        height: 50,
        flexDirection: 'row'
    },
    leftButton: {
        width: 40,
        height: 40,
        position: 'absolute',
        right: -10
    },
    refresh: {
        marginTop: 15,
        resizeMode: 'cover'
    },

    leftButtonIcon: {
        width: 20,
        height: 20,
        tintColor: ColorsConstant.White,
        marginBottom: 5

    },
    rightButton: {
        minWidth: 50,
        height: 50,
        marginLeft: 'auto'
    },
    headerTitle: {
        color: ColorsConstant.White,
        fontSize: 16,
        marginTop: 4,
        fontFamily: "Poppins-Regular",
        paddingTop: 10,
        maxWidth: screenWidth - 100,
        textAlign: 'left'
    }
})